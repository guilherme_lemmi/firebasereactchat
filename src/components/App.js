import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AltContainer from 'alt-container';
import injectTapEventPlugin from 'react-tap-event-plugin';

import LoginPage from './login/LoginPage';
import ChatPage from './chat/ChatPage';

import UserActions from '../actions/UserActions';
import UserStore from '../stores/UserStore';
import ChatActions from '../actions/ChatActions';
import ChatStore from '../stores/ChatStore';
import MessageActions from '../actions/MessageActions';
import MessageStore from '../stores/MessageStore';

import css from '../styles/App.css';

injectTapEventPlugin();

class App extends React.Component {

	constructor(props) {
		super(props);
		this.onChange = this.onChange.bind(this);
		
		this.state = UserStore.getState();
	}

	componentDidMount() {
    UserActions.fetchCurrentUser(ChatActions.fetchPrivateChats);
    UserStore.listen(this.onChange);
  }

  onChange(state) {
    this.setState(state);
  }

	handleNotImplemented(){
    alert("Não consegue...");
  };

	render() {
		let content;

		if (!this.state.user) {

			content = (
				<AltContainer 
					store={UserStore}
					actions={{UserActions: UserActions}}
				>
					<LoginPage />
				</AltContainer>
			);

		} else {

			let actions = {
				UserActions: UserActions,
				ChatActions: ChatActions,
				MessageActions: MessageActions
			};
			let stores = {
				UserStore: UserStore,
				ChatStore: ChatStore,
				MessageStore: MessageStore
			};

			content = (
				<AltContainer
					stores={stores}
					actions={actions}
				>
					<ChatPage />
				</AltContainer>
				
			);

		}

		return (
			<MuiThemeProvider>
				{content}
			</MuiThemeProvider>
		);
	}
}

export default App;
