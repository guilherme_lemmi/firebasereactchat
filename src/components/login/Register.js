import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class Register extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      name: '',
      email: '',
      password: ''
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }
  
  handleNameChange(e) {
    this.setState({
      name: e.target.value
    });
  }

  handleEmailChange(e) {
    this.setState({
      email: e.target.value
    });
  }

  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleRegister() {
    this.props.onSubmit(
      this.state.name,
      this.state.email,
      this.state.password
    );
  }

  render() {
    return (
      <div>
        <TextField
          hintText="Sua graça"
          floatingLabelText="Nome"
          fullWidth={true}
          onChange={this.handleNameChange}
        />
        <TextField
          hintText="Email"
          floatingLabelText="Email"
          type="email"
          fullWidth={true}
          onChange={this.handleEmailChange}
        />
        <TextField
          hintText="Senha"
          floatingLabelText="Senha"
          type="password"
          fullWidth={true}
          onChange={this.handlePasswordChange}
        />
        <RaisedButton label="Cadastrar"
          primary={true}
          fullWidth={true}
          onClick={this.handleRegister} />
      </div>
    );
  }
}

export default Register;