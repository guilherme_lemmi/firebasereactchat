import React from 'react';
import {Card, CardText} from 'material-ui/Card';
import {Tabs, Tab} from 'material-ui/Tabs';
import LoginHeader from './LoginHeader';
import Login from './Login';
import Register from './Register';

class LoginPage extends React.Component {

  constructor(props) {
    super(props);

    this.handleLogin = this.handleLogin.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
  }

  handleLogin(email, password) {
    this.props.UserActions.login(email, password);
  }

  handleRegister(name, email, password) {
    this.props.UserActions.register(name, email, password);
  }

  render() {
    return (
      <div className="login-page">
        <LoginHeader />
        <main className="login-main center-xs row">
          <h2 className="col-xs-12">Oooooolá Marilene!</h2>
          <h3 className="col-xs-12">Me diga seu email e senha e vamos conversar!</h3>
          <Card className="login-container col-xs-12">
            <CardText>
               <Tabs className="tabs" contentContainerClassName="tabs-labels">
                <Tab label="Já te conheço" buttonStyle={{backgroundColor: '#004D40'}} >
                  <Login onSubmit={this.handleLogin}/>
                </Tab>
                <Tab label="Não te conheço" buttonStyle={{backgroundColor: '#004D40'}} >
                  <Register onSubmit={this.handleRegister} />
                </Tab>
              </Tabs>
            </CardText>
          </Card>
        </main>
      </div>
    );
  }
}

export default LoginPage;