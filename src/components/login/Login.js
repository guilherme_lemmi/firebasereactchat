import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleEmailChange(e) {
    this.setState({
      email: e.target.value
    });
  }

  handleSubmit() {
    this.props.onSubmit(this.state.email, this.state.password);
  }

  render() {
    return (
      <div>
        <TextField
          hintText="Email"
          floatingLabelText="Email"
          type="email"
          fullWidth={true}
          onChange={this.handleEmailChange}
        />
        <TextField
          hintText="Senha"
          floatingLabelText="Senha"
          type="password"
          fullWidth={true}
          onChange={this.handlePasswordChange}
        />
        <RaisedButton label="Entrar"
          primary={true}
          fullWidth={true}
          onClick={this.handleSubmit} />
      </div>
    );
  }
}

export default Login;