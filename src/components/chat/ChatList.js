import React from 'react';
import _ from 'underscore';
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import {List, ListItem} from 'material-ui/List';

class ChatList extends React.Component {
 
  render() {
    return (
      <div className="chat-chats">
        <List>
          <Subheader>Conversas recentes</Subheader>
          {_.map(this.props.privateChats, (chat, id) => {
            return (
              <ListItem
                key={id}
                primaryText={chat.name}
                leftAvatar={<Avatar src="olamarilene.png" />}
                secondaryText="yolo"
              />
            )}
          )}
        </List>
        <Divider inset={true} />
        <List>
          <Subheader>Outras conversas</Subheader>
          {_.map(this.props.publicChats, (chat, id) => {
            return (
              <ListItem
                key={id}
                primaryText={chat.name}
                leftAvatar={<Avatar src="olamarilene.png" />}
                secondaryText="yolo"
              />
            )}
          )}
        </List>
      </div>
    );
  }
}

export default ChatList;