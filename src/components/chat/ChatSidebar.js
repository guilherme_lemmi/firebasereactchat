import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContactList from './ContactList';

class ChatSidebar extends React.Component {
 
  render() {
    return (
      <div>
        <div className="top">
          <div className="search">
            <input type="text" onChange={this.props.onNotImplemented} />
            <FloatingActionButton 
              style={{margin: '0 5px 5px 20px', float: 'right'}}
              mini={true}
              zDepth={1}
              onTouchTap={this.props.onNotImplemented}
            >
              <ContentAdd />
            </FloatingActionButton>
          </div>
        </div>
        <div className="people">
          <Tabs className="tabs">
            <Tab label="Contatos" >
              <ContactList 
                user={this.props.user}
                users={this.props.users}
                onGoToUserChat={this.props.onGoToUserChat}
              />
            </Tab>
            <Tab label="Recentes" onClick={this.props.onNotImplemented}>
            </Tab>
          </Tabs>
        </div>
        <a href="" onClick={this.props.onLogout} >Sair</a>
      </div>
    );
  }
}

export default ChatSidebar;