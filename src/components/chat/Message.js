import React from 'react';
import {Card, CardText} from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import NavigationMore from 'material-ui/svg-icons/navigation/more-vert';
import moment from 'moment';

class Message extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleTouchTap = (event) => {
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  handleDeleteMessage = () => {
    this.props.onDeleteMessage(this.props.chatId, this.props.messageId);
  };

  render() {
    console.log(this.props.author);
    console.log(this.props.user.id);
    let authorName = this.props.author ? this.props.author.name : '';
    let avatar = !this.props.isAuthor ? <Avatar style={{marginRight:20}}>{authorName.substr(0,1)}</Avatar> : '';
    return (
      <div className="row" style={{margin:'0 20px'}}>
        {avatar}
        <div
          className={'bubble row ' + (this.props.isAuthor ? 'me' : 'you') }
          style={{display:'flex'}}>
          <div className="col-md-11">
            <strong>{authorName}</strong>
            <p style={{margin:0}}>{this.props.message.body}</p>
          </div>
          <div className="col-md-1" style={{textAlign:'right',color:'white'}}>
            <IconButton tooltip="More..."
                        onTouchTap={this.handleTouchTap}
                        style={{padding: 0, height:10, color: '#fff'}}>
              <NavigationMore style={{color:'#fff'}}/>
            </IconButton>
            <Popover
              open={this.state.open}
              anchorEl={this.state.anchorEl}
              anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              onRequestClose={this.handleRequestClose}
            >
              <Menu>
                <MenuItem
                  primaryText="Reply"
                  onTouchTap={this.handleNotImplemented}
                />
                <MenuItem
                  primaryText="Pin" 
                  onTouchTap={this.handleNotImplemented}
                />
                <MenuItem 
                  primaryText="Edit"
                  onTouchTap={this.handleNotImplemented}
                />
                <MenuItem 
                  primaryText="Delete"
                  onTouchTap={this.handleDeleteMessage}
                />
              </Menu>
            </Popover>
          </div>
        </div>
        <div className="col-md-1" style={{marginLeft:10}}>
          {moment.unix(this.props.message.createdAt/1000).format('HH:mm')}
        </div>
      </div>
    );
  }
}

export default Message;