import React from 'react';

class InputBox extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      text: ''
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTextChange(e) {
    this.setState({
      text: e.target.value
    });
  }

  handleSubmit() {
    this.props.onSubmit(this.state.text);
	}

  render() {
    return (
      <div className="write input-box-container">
        <a href="javascript:;" className="write-link attach"></a>
        <input 
          type="text"
          className="col-md-11"
          onChange={this.handleTextChange}
        />
        <a href="javascript:;" className="write-link send" onClick={this.handleSubmit}></a>
      </div>
    );
  }
}

export default InputBox;

