import React from 'react';
import AppBar from 'material-ui/AppBar';
import Snackbar from 'material-ui/Snackbar';
import MainMenu from './MainMenu';
import ChatMain from './ChatMain';
import ChatSidebar from './ChatSidebar';

import ChatCss from '../../styles/Chat.css';

class ChatPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };

    this.handleLogout = this.handleLogout.bind(this);
    this.handleGoToUserChat = this.handleGoToUserChat.bind(this);
    this.handleNotImplemented = this.handleNotImplemented.bind(this);
  }

  componentDidMount() {
    this.props.ChatActions.fetchPublicChats();
    this.props.UserActions.fetchUsers();
  }

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  handleLogout() {
    console.log('logout');
    this.props.UserActions.logout();
  }

  handleGoToUserChat(userId) {
    const params = [
      this.props.UserStore.user.id,
      userId,
      this.props.MessageActions.fetchMessagesForChat
    ];
    this.props.ChatActions.fetchUserChat(...params);
  }
  
  handleNotImplemented() {
    this.setState({
      open: true,
    });
  };

  render() {
    return (
      <div>
        <div className="container">
          <div className="left">
            <ChatSidebar 
              user={this.props.UserStore.user}
              users={this.props.UserStore.users}
              privateChats={this.props.ChatStore.privateChats}
              publicChats={this.props.ChatStore.publicChats}
              onGoToUserChat={this.handleGoToUserChat}
              onNotImplemented={this.handleNotImplemented}
              onLogout={this.handleLogout}
            />
          </div>
          <div className="right">
            <ChatMain
              user={this.props.UserStore.user}
              chat={this.props.ChatStore.selectedChat}
              users={this.props.UserStore.users}
              messages={this.props.MessageStore.messages}
              onAddMessage={this.props.MessageActions.addMessage}
              onDeleteMessage={this.props.MessageActions.deleteMessage}
              onNotImplemented={this.handleNotImplemented}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ChatPage;