import React from 'react';
import _ from 'underscore';
import CircularProgress from 'material-ui/CircularProgress';
import Message from './Message';

class MessageList extends React.Component {

  render() {
    if (!this.props.messages) {
      return (
        <CircularProgress
          mode="indeterminate"
          style={{
            paddingTop: '160px',
            paddingBottom: '20px',
            margin: '0 auto',
            display: 'block',
            width: '60px'
          }}
        />
      );
    }

    return (
      <div style={{overflowY:'scroll',width: '100%',alignSelf: 'flex-end'}}>
        <div className="messages-list">
          {_.map(this.props.messages, (message, id) => {
            let author = message.userId ? this.props.users[message.userId] : '';
            return (
              <Message key={id}
                chatId={this.props.chat.id}
                author={author}
                isAuthor={author.id == this.props.user.id}
                user={this.props.user}
                message={message}
                messageId={id}
                onDeleteMessage={this.props.onDeleteMessage}
              />
            )}
          )}
        </div>
      </div>
    );
  }
}

export default MessageList;