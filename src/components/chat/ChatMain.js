import React from 'react';
import * as firebase from 'firebase';
import MessageList from './MessageList';
import InputBox from './InputBox';

class ChatMain extends React.Component {

  constructor(props) {
    super(props);
    this.handleAddMessage = this.handleAddMessage.bind(this);
  }

  handleAddMessage(msgText) {
    const msg = {
      userId: this.props.user.id,
      body: msgText,
      createdAt: firebase.database.ServerValue.TIMESTAMP
    };
    this.props.onAddMessage(this.props.chat.id, msg);
  }
  
  render() {
    return (
      <div className="chat-main chat active-chat">
				<MessageList
          chat={this.props.chat}
          user={this.props.user}
          users={this.props.users}
          messages={this.props.messages}
          onDeleteMessage={this.props.onDeleteMessage}
        />
        <InputBox onSubmit={this.handleAddMessage}/>
      </div>
    );
  }
}

export default ChatMain;