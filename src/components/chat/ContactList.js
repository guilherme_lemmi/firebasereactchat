import React from 'react';
import _ from 'underscore';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';

class ContactList extends React.Component {
 
  constructor(props) {
    super(props);
    this.createContactTapHandler = this.createContactTapHandler.bind(this);
  }

  createContactTapHandler(userId) {
    return () => {
      console.log(userId);
      this.props.onGoToUserChat(userId);
    }
  }

  render() {
    return (
      <div className="chat-contacts">
        <List>
          {_.map(this.props.users, (user, id) => {
            if (id !== this.props.user.id) {
              return (
                <ListItem key={id}
                  primaryText={user.name}
                  leftAvatar={<Avatar src="olamarilene.png" />}
                  secondaryText={user.quote}
                  onTouchTap={this.createContactTapHandler(id)}
                />
              );
            }
          })}
        </List>
      </div>
    );
  }
}

export default ContactList;