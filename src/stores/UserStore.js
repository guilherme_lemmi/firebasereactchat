import alt from '../alt';
import UserActions from '../actions/UserActions';

class UserStore {

  constructor() {
    this.state = {
      user: {},
      users: {}
    };
    
    this.bindActions(UserActions);
  }

  onFetchCurrentUser(user) {
    this.setState({ user: user });
  }

  onFetchUsers(users) {
    this.setState({ users: users });
  }

}

export default alt.createStore(UserStore, 'UserStore');