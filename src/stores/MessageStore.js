import alt from '../alt';
import MessageActions from '../actions/MessageActions';

class MessageStore {

  constructor() {
    this.state = {
      messages: {},
    };
    
    this.bindActions(MessageActions);
  }

  onAddMessage(message) {
    console.log('onAddMsg');
  }

  onFetchMessagesForChat(messages) {
    this.setState({
      messages: messages
    });
  }

  onMessagesFailed(errorMessage) {
    this.errorMessage = errorMessage;
  }
}

export default alt.createStore(MessageStore, 'MessageStore');