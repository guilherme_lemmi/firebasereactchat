import alt from '../alt';
import ChatActions from '../actions/ChatActions';

class ChatStore {

  constructor() {
    this.state = {
      privateChats: {},
      publicChats: {},
      selectedChat: null
    };
    this.bindActions(ChatActions);
  }

  onFetchUserChat(chat) {
    console.log('chatstore');
    console.log(chat);
    this.setState({
      selectedChat: chat
    });
  }

  onFetchPublicChats(chats) {
    let newState = { publicChats: chats };
    if (chats && !this.state.selectedChat) {
      newState.selectedChat = Object.keys(chats)[0];
    }
    this.setState(newState);
  }

  onFetchPrivateChats(chat) {
    this.state.privateChats[chat.key] = chat.val();
    this.setState({
      privateChats: this.state.privateChats
    });
  }

}

export default alt.createStore(ChatStore, 'ChatStore');