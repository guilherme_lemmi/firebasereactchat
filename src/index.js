import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './styles/index.css';
import * as firebase from 'firebase';

const config = {
	apiKey: "AIzaSyBbgCl1g6_vimDqOFpnnpXt2tsf9W-MPj4",
	authDomain: "fir-reactchat.firebaseapp.com",
	databaseURL: "https://fir-reactchat.firebaseio.com",
	projectId: "fir-reactchat",
	storageBucket: "fir-reactchat.appspot.com",
	messagingSenderId: "122810556090"
};

firebase.initializeApp(config);

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
