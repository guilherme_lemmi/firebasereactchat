import alt from '../alt';
import * as firebase from 'firebase';

class UserActions {

  login(email, password) {
    return (dispatch) => {
      firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        console.log(error.code, error.message);
      });
      dispatch();
    };
  }

  logout() {
    return (dispatch) => {
      firebase.auth().signOut().then(function() { }, function(error) {});
      dispatch();
    };
  }

  register(name, email, password) {
    return (dispatch) => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((user) => {
          this.addUser({
            id: user.uid,
            name: name,
            email: email,
            createdAt: firebase.database.ServerValue.TIMESTAMP
          });
        }).catch(function(error) {
          console.log(error.code, error.message);
        });
      dispatch();
    };
  }

  addUser(user) {
    return (dispatch) => {
      const usersRef = firebase.database().ref().child('users');
      usersRef.child(user.id).set(user);
      dispatch();
    };
  }

  fetchCurrentUser(onUserLoaded) {
    return (dispatch) => {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
          onUserLoaded(user.uid);
          const userRef = firebase.database().ref().child('users');
          userRef.child(user.uid).on('value', snap => {
            dispatch(snap.val());
          });
        } else {
          dispatch(null);
        }
      });
    }
  }

  fetchUsers() {
    return (dispatch) => {
      const usersRef = firebase.database().ref().child('users');
      usersRef.on('value', snap => {
        dispatch(snap.val());
      });
    }
  }

}

export default alt.createActions(UserActions);