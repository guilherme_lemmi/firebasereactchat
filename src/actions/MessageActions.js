import alt from '../alt';
import * as firebase from 'firebase';

class MessageActions {

  addMessage(chatId, msg) {
    return (dispatch) => {
      const msgsRef = firebase.database().ref().child('chat-messages');
      const msgRef = msgsRef.child(chatId).push();
      msg.id = msgRef.key;
      msgRef.set(msg);
      dispatch();
    };
  }

  deleteMessage(chatId, msgId) {
    return (dispatch) => {
      const msgsRef = firebase.database().ref().child('chat-messages');
      msgsRef.child(chatId).child(msgId).remove();
      dispatch();
    };
  }

  fetchMessagesForChat(chatId) {
    if (chatId) {
      return (dispatch) => {
        const msgsRef = firebase.database().ref().child('chat-messages').child(chatId);
        msgsRef.on('value', snap => {
          dispatch(snap.val());
        });
      }
    }
  }

  messagesFailed(errorMessage) {
    return (dispatch) => {
      dispatch(errorMessage);
    };
  }
}

export default alt.createActions(MessageActions);