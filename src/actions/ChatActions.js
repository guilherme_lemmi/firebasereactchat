import alt from '../alt';
import * as firebase from 'firebase';

class ChatActions {

  fetchPrivateChats(userId) {
    return (dispatch) => {
      const userChatsRef = firebase.database().ref().child('user-chats');
      const chatsRef = firebase.database().ref().child('chats');
      userChatsRef.child(userId).on('child_added', snap => {
        let chatRef = chatsRef.child(snap.key);
        chatRef.once('value', snap => {
          dispatch(snap);
        });
      });
    }
  }

  fetchPublicChats() {
    return (dispatch) => {
      const chatsRef = firebase.database().ref().child('chats')
                        .orderByChild('privacy').startAt('public').endAt('public');
      chatsRef.on('value', snap => {
        dispatch(snap.val());
      });
    }
  }

  addUserChat(uid1, uid2) {
    
  }

  fetchUserChat(uid1, uid2, onUserChatFetched) {
    return (dispatch) => {
      const userToUserRef = firebase.database().ref().child('user-to-user');
      const chatsRef = firebase.database().ref().child('chats');
      userToUserRef.child(uid1).child(uid2).once('value').then(snap => {
        if (snap.val()) {
          let chatRef = chatsRef.child(snap.val());
          chatRef.once('value').then(snap => {
            onUserChatFetched(snap.key);
            dispatch(snap.val());
          });
        } else {
          //there's no user-to-user chat yet, let's create one
          const chatRef = chatsRef.push();
          const chat = {
            id: chatRef.key,
            name: uid1 + '_' + uid2,
            createdAt: firebase.database.ServerValue.TIMESTAMP
          };
          chatRef.set(chat);

          userToUserRef.child(uid1).child(uid2).set(chatRef.key);
          userToUserRef.child(uid2).child(uid1).set(chatRef.key);
          onUserChatFetched(chatRef.key);
          dispatch(chat);
        }
      });
    }
  }

}

export default alt.createActions(ChatActions);