var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: ['./src/App.js'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
      loaders: [
          { 
			test: /\.js$/,
			exclude: /node_modules/,
			loader: "babel-loader",
			query: {presets:['es2015','react']}
		  },
		  {
			test: /\.css$/,
			loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' })
		  },
		  {
			test: /\.scss$/,
			loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!sass-loader' })
		  },
		  {
			test: /\.(png|woff|woff2|eot|otf|ttf|svg|jpg|gif)$/,
			loader: 'file-loader?name=img/[name].[ext]'
		  },
		  {
			test: /\.html$/,
			loader: 'html-loader'
		  }
      ]
  },
  plugins: [
        new ExtractTextPlugin({ filename: 'style.css', 
            allChunks: true, disable: false
        }),
		new HtmlWebpackPlugin({
		  inject: true,
		  template: 'src/index.html'
		}),
  ]
};